import * as NetSync from '../../dist/NetSync.js';

"use strict";

class PlayerEntity extends NetSync.BaseEntity {
    init(p) {
        this.map(['x', 'y', 'direction']);

        this.speed = 400; // Per second
        this.radius = 30;

        this.x = p.x || 100;
        this.y = p.y || 100;
        this.direction = p.direction || [1,0];
        this.setOwner(p.owner);
    }

    processInput(playerId, input, fromServer) {
        if (this.owner != playerId) return;
        switch (input.key) {
            case 'w':
                this.y -= (input.ms/1000)*this.speed;
                this.direction = [0,-1];
                break;
            case 'a':
                this.x -= (input.ms/1000)*this.speed;
                this.direction = [-1,0];
                break;
            case 's':
                this.y += (input.ms/1000)*this.speed;
                this.direction = [0,1];
                break;
            case 'd':
                this.x += (input.ms/1000)*this.speed;
                this.direction = [1,0];
                break;
            case 'enter':
                if (input.down) this.world.serverAddEntity('BaseProjectile', {direction: this.direction.slice(), x: this.x, y: this.y, source: this.owner});
                break;
            default:
                break;
        }
    }

    handleEvent(event) {
        switch (event.name) {
            case 'player-removed':
                if (this.owner == event.playerId) this.world.serverRemoveEntity(this.entityId);
                break;
        }
    }
}

class BaseProjectile extends NetSync.BaseEntity {
    init(p) {
        this.map(['x', 'y', 'source', 'direction']);
        this.speed = 500; // Per second
        this.radius = 3;

        this.source = p.source;
        this.x = p.x;
        this.y = p.y;
        this.direction = p.direction;
    }

    handleEvent(event) {
        switch (event.name) {
            case 'tick':
                this.x += this.direction[0] * (this.speed * (event.ms/1000));
                this.y += this.direction[1] * (this.speed * (event.ms/1000));

                this.world.entities.forEach(entity => {
                    if (entity.entityType() != 'PlayerEntity') return;
                    if (this.source == entity.owner) return;
                    var distance = Math.sqrt( (entity.x-this.x)*(entity.x-this.x) + (entity.y-this.y)*(entity.y-this.y) );
                    if (distance <= entity.radius) {
                        this.world.serverRemoveEntity(entity.entityId);
                    }
                });
                break;
            default:
                break;
        }
    }
}

class GameWorld extends NetSync.BaseWorld {
    onMessage(msg, conn) {
        if (this.isServer) {
            switch (msg.type) {
                case 'join':
                    //this.serverAddPlayer('SamplePlayer', null, conn.id, null);
                    this.addPlayer(null, conn.id);
                    break;
                default:
                    break;
            }
        } else {
            switch (msg.type) {
                case 'own-player':
                    var input = new NetSync.SampleInput();
                    this.assignInput(input, msg.playerId);
                    break;
                default:
                    break;
            }
        }
    }

    onEvent(event) {
        switch (event.name) {
            case 'player-added':
                this.serverAddEntity('PlayerEntity', {owner: event.playerId});
                break;
            default:
                break;
        }
    }

    ready() {
        if ( ! this.isServer ) this.gateway.lastConn.send({type: 'join'});
    }

    afterTick() {
        this.render();
    }

    render() {
        if (this.isServer) return;
        if ( ! ctx ) return;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        this.entities.forEach(entity => {
            var radius = (entity.radius);
            ctx.beginPath();
            ctx.arc(entity.x, entity.y, radius, 0, 2*Math.PI);
            ctx.stroke();
        });
    }
}

var world;
var ctx;
var canvas;
if (typeof window == 'undefined') {
    world = new GameWorld(true);
} else {
    world = new GameWorld(false);
    window.addEventListener("load", function (event) {
        canvas = document.getElementById('game');
        var body = document.getElementsByTagName('body')[0];
        body.appendChild(canvas);
        ctx = canvas.getContext('2d');
    });
}

world.registerEntityType(PlayerEntity);
world.registerEntityType(BaseProjectile);
//world.registerPlayerType(NetSync.SamplePlayer);

var PORT = 8085;
var gateway = new NetSync.TCPGateway(PORT);
world.setGateway(gateway);
gateway.enable();

world.start(1000/30);

setTimeout(function () {
    world.logState();
}, 4000);

