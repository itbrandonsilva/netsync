"use strict";

export class BaseWorld {
    constructor(isServer, debug) {
        this.mode = debug ? 'debug' : undefined;
        this.isServer = isServer;
        this.state = {};
        this.idPool = 0;
        this.maxPlayers = 8;
        this.players = new Map();
        this.entityTypes = {};
        this.playerTypes = {};
        this.entities = new Map();
        this.inputs = new Map();

        this.tickHistory = [];
        this.serverTickHistory = [];

        this.ticks = 0;
    }

    debug(log) {
        if (this.mode == 'debug') console.log(log);
    }

    genId() {
        return ((this.isServer ? "s-" : "c-" ) + (++this.idPool));
    }

    registerEntityType(clss) {
        this.entityTypes[clss.name] = clss;
    }

    setGateway(gateway) {
        this.connected = false;
        this.gateway = gateway;
        gateway.setWorld(this);
    }

    gatewayReady() {
        this.connected = true;
        console.log('Gateway is ready.');
        this.ready();
    }
    ready() {};

    gatewayDisconnected(error) {}
    receiveConnection(conn) {
        conn.send({type: 'world-snapshot', snapshot: this.createWorldSnapshot()});
    }
    lostConnection(conn, err) {
        if (err) console.log(err);
        this.players.forEach((player, id) => {
            if (player.connId == conn.id) this.removePlayer(id);
        });
    }

    // Handles game logic from the gateway
    handleMessage(msg, conn) {
        switch (msg.type) {
            case 'tick':
                if (msg.tickHistory.length) this.addServerHistory(msg.tickHistory);
                break;
            case 'world-snapshot':
                this.restoreWorldSnapshot(msg.snapshot);
                break;
            case 'disown-player':
                this.inputs.get(msg.id).destroy();
                this.inputs.delete(msg.id);
                this.onMessage(msg, conn);
            default:
                this.onMessage(msg, conn);
                break;
        }  
    }
    onMessage(msg, conn) {};

    addPlayer(id, connId) {
        if ( ! this.isServer ) return;
        id = id || (() => {
            let id = 1;
            while (this.players.has(id)) ++id;
            return id;
        }());
        if (this.players.has(id)) throw new Error('Player id ' + id + ' is in use.');

        this.players.set(id, {});
        this.assignPlayer(id, connId);
        this.fireEvent('player-added', {playerId: id});
        return id;
    }

    removePlayer(id) {
        if ( ! this.isServer ) return;
        var player = this.players.get(id);
        if ( ! player ) throw new Error('Invalid id specified at removePlayer()');

        this.unassignPlayer(id);
        this.players.delete(id);
        this.fireEvent('player-removed', {playerId: id});
    }

    assignPlayer(playerId, connId) {
        var player = this.players.get(playerId);
        if ( ! player ) throw new Error('Invalid id specified at assignPlayer()');
        player.connId = connId;

        var msg = {type: 'own-player', playerId};
        var conn = this.gateway.connections[connId]
        if (conn) conn.send(msg);
        else this.handleMessage(msg);
    }

    unassignPlayer(playerId) {
        var player = this.players.get(playerId);
        if ( ! player ) throw new Error('Invalid id specified at unassignPlayer()');

        var msg = {type: 'disown-player', playerId}
        var conn = this.gateway.connections[player.connId];
        delete player.connId;
        if (conn) conn.send();
        else this.handleMessage(msg);
    }

    assignInput(source, playerId) {
        this.inputs.set(playerId, source);
    }

    getPlayer(id) {
        return this.players.get(id);
    }

    // Using this instead of holding onto entity references may not be best for performance, but better to
    // prevent leaks.
    getEntity(entityId) {
        return this.entities.get(entityId);
    }

    removeEntity(entityId) {
        var entity = this.getEntity(entityId);
        if ( ! entity ) throw new Error('Synchronization error, attempted to remove an entity that does not exist');
        entity.destroy();
        this.entities.delete(entityId);
    }

    ensureEntity(params) {
        params.entityId = params.entityId || (this.isServer ? this.genId() : null);
        if ( ! params.entityId ) throw new Error('Invalid entityId passed to ensureEntity()');
        var entity = this.getEntity(params.entityId);
        if (entity) return entity.construct(params);

        params.world = this;
        entity = new this.entityTypes[params.entityType](params);
        this.entities.set(params.entityId, entity);
        return entity;
    }

    serverAddEntity(entityType, constructor) {
        if ( ! this.isServer ) return;
        constructor.entityType = entityType;
        this.fireEvent('entity-create', {constructor: constructor});
    }

    serverRemoveEntity(entityId) {
        if ( ! this.isServer ) return;
        this.fireEvent('entity-remove', {entityId: entityId});
    }

    fireEvent(name, event) {
        if ( ! this.isServer ) return;

        var tickElement = event;
        tickElement.type = 'event';
        tickElement.name = name;
        this.tickHistory.unshift(tickElement);
    }

    handleEvent(event) {
        if (event.name != 'tick') this.debug(event);
        switch (event.name) {
            case 'entity-create':
                var entity = this.ensureEntity(event.constructor);
                if (this.isServer) {
                    var snapshot = entity.createSnapshot();
                    event.constructor = snapshot;
                }
                this.fireEvent('entity-created', {entityId: entity.entityId});
                break;
            case 'entity-remove':
                this.removeEntity(event.entityId);
                this.fireEvent('entity-removed', {entityId: event.entityId});
                break;
            default:
                this.onEvent(event);
                break;
        }
    }
    onEvent(event) {};

    _beforeTick(ms) {
        if (this.gateway && ( ! this.connected )) return;

        var inputs = [];
        this.inputs.forEach((source, playerId) => {
            var sourceInputs = [...source.inputs, ...source.sampleInputs()];
            sourceInputs = sourceInputs.map(input => { return {type: 'input', playerId, input}; });
            inputs = [...inputs, ...sourceInputs];
        });

        this.fireEvent('tick', {ms: ms});

        this.tickHistory = inputs.concat(this.tickHistory);

        // Get our inputs to the server as fast as possible
        if ( ! this.isServer ) this.emitTickHistory();

        // Merge any history from remote instances into tickHistory for processing
        this.mergeTickHistory();

        // Ensure this is not needed
        //this.tickHistory = this.tickHistory.concat(inputs);
        this.beforeTick();
    }

    tick(ms) {
        this._beforeTick(ms);

        var history = [];
        var tickElement;
        while (tickElement = this.tickHistory.shift()) {
            history.push(tickElement);

            if (tickElement.type == 'event') {
                this.handleEvent(tickElement);
                this.entities.forEach(entity => {
                    entity.handleEvent(tickElement);
                });
                continue;
            }

            if (tickElement.type == 'input') {
                this.entities.forEach(entity => {
                    if ( ! entity.shouldProcessInput(tickElement.server) ) return;
                    entity.processInput(tickElement.playerId, tickElement.input);
                });
                continue;
            }

            throw new Error("Unable to handle tickAction: " + tickAction);
        };

        this.tickHistory = history;
        this._afterTick(ms);
    }

    _afterTick(ms) {
        if (this.isServer) this.emitTickHistory();
        this.tickHistory.length = 0;
        this.afterTick();
    }

    beforeTick(ms) {};
    afterTick(ms) {};

    emitTickHistory() {
        if ( this.isServer ) {
            this.tickHistory.forEach(function (tickAction) {
                if (tickAction.type == 'input') tickAction.server = true;
            });
        }

        this.gateway.emit({type: 'tick', tickId: ++this.ticks, tickHistory: this.tickHistory});
    }

    addServerHistory(serverHistory) {
        if (this.isServer) serverHistory = serverHistory.filter(function (tickElement) {
            if (tickElement.type == 'event') return false;
            return true;
        });
        this.serverTickHistory = this.serverTickHistory.concat(serverHistory);
    }

    mergeTickHistory() {
        this.tickHistory = this.serverTickHistory.splice(0).concat(this.tickHistory);
    }

    start(rate) {
        this.tick(0);
        var then = new Date().getTime();
        setInterval(() => {
            var now = new Date().getTime();
            this.tick(now-then);
            then = now;
        }, rate || 1000/2);
    }

    createWorldSnapshot() {
        if ( ! this.isServer ) throw new Error('Client attempted to create a world snapshot.');
        var snapshot = {entities: [], state: this.state};
        this.entities.forEach((entity, entityId) => {
            snapshot.entities.push(entity.createSnapshot());
        });
        return snapshot;
    }

    restoreWorldSnapshot(snapshot) {
        this.state = snapshot.state;
        snapshot.entities.forEach(snapshot => {
            this.ensureEntity(snapshot);
        });
    }

    logState() {
        return;
        console.log('');
        console.log('-------------------------------------------');
        console.log('State log');

        console.log('Players:');
        this.players.forEach((player, id) => {
            if (player) console.log('    ', player);
        });

        console.log('Entities:');
        this.entities.forEach(entity => {
            console.log('    ', entity.createSnapshot());
        });

        console.log('Gateway:');
        console.log('    ', this.gateway.connId);
    }
}
