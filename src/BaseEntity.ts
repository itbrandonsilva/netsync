"use strict";

export class BaseEntity {
    constructor(params) {
        this.construct(params);
    }

    construct(params) {
        this._map = ['inputType', 'entityId', 'owner'];
        params = params || {};
        if (params.world) {
            this.world = params.world;
            delete params.world;
        }
        this.setId(params.entityId);
        this.init(params);
        this.inputType = this.inputType || 'serverOnly';
    }
    init(params) {};

    entityType() {
        return this.constructor.name;
    }

    //static entityType() {
    //    return 'BaseEntity';
    //}

    map(props) {
        this._map.push.apply(this._map, props);
    }

    //entityType() {
    //    return this.constructor.entityType();
    //}

    setId(id) {
        if ( ! id ) return;
        this.entityId = id;
    }

    setOwner(playerId) {
        this.owner = playerId;
    }

    getOwner() {
        return this.world.getPlayer(this.owner);
    }

    handleEvent(event) {};

    shouldProcessInput(fromServer) {
        switch (this.inputType) {
            case 'serverOnly':
                if (fromServer || this.world.isServer) return true;
            case 'none':
            default:
                return false;
        }
    }
    processInput(playerId, input) {}

    createSnapshot(snapshot) {
        snapshot = snapshot || {};
        this._map.forEach(property => {
            snapshot[property] = this[property];
        });
        snapshot.entityType = this.entityType();
        return snapshot;
    }

    //restoreSnapshot(snapshot) {
    //    this._map.forEach(property => {
    //        this[property] = snapshot[property];
    //    });
    //}

    destroy() {
        delete this.world;
    }

    setInputType(type) { this.inputType = this.inputType || 'serverOnly'; }
}
