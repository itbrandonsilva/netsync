"use strict";

export class BaseGateway {
    setWorld(world) {
        this.connections = {};
        this.world = world;
    }

    emit(msg) {
        if ( ! this.world.isServer ) return this.lastConn.send(msg);
        for (var connId in this.connections) {
            this.connections[connId].send(msg);
        }
    }

    handleMessage(msg, conn) {
        switch (msg.type) {
            case 'connection-id':
                if (this.world.isServer) throw new Error('Server received a connection-id (should be impossible)');
                this.connId = msg.id;
                return true;
                break;
            default:
                this.world.handleMessage(msg, conn);
                break;
        }
    }

    registerConnection(sendHandler) {
        var id = new Date().getTime();
        var conn = {id: id, send: sendHandler};
        this.connections[id] = conn;
        this.lastConn = conn;
        if (this.world.isServer) {
            conn.send({type: 'connection-id', id: id});
            this.world.receiveConnection(conn);
        }
        return conn;
    }

    destroyConnection(conn, err) {
        if (err) console.log(err);
        this.world.lostConnection(conn, err);
        if (this.lastConn == conn) delete this.lastConn;
        delete this.connections[conn.id];
    }

    gatewayReady() {
        this.ready = true;
        this.world.gatewayReady();
    }
};
