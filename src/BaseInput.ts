"use strict";

export class BaseInput {
    constructor() {
        this.inputs = [];
    }

    addInput(data) {
        this.inputs.push(data);
    }

    // Override this to have logic that provides inputs
    sampleInputs() {}

    //newInput(data) {
    //    return {type: 'input', /*inputId: this.world.genId(),*/ input: data};
    //}

    // Override this to clean up references
    destroy() {};
}
