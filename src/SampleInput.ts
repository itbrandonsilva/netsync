import {BaseInput} from "./BaseInput.js";

"use strict";

export class SampleInput extends BaseInput {
    constructor(keys) {
        super();
        this.keys = keys || ['w', 'a', 's', 'd', 'enter'];

        var keysDown = {};
        var keyHistory = [];
        this.keysDown = keysDown;
        this.keyHistory = keyHistory;

        var config = [];
        this.keys.forEach(key => {
            config.push({
                "keys"          : key,
                "prevent_repeat": true,
                "on_keydown"    : () => {
                    keyHistory.push({key: key, ms: 0, down: true});
                    var ts = new Date().getTime();
                    keysDown[key] = ts;
                },
                "on_keyup"      : () => {
                    var ts = new Date().getTime();
                    keyHistory.push({key: key, ms: ts-keysDown[key], up: true});
                    delete keysDown[key];
                },
            });
        })

        var interval = setInterval(() => {
            if (document.readyState != "complete") return;
            var listener = new keypress.Listener();
            var my_combos = listener.register_many(config);
            clearInterval(interval);
        }, 25);
    }

    sampleInputs() {
        var ts = new Date().getTime();
        for (var key in this.keysDown) {
            this.keyHistory.push({key: key, ms: ts-this.keysDown[key]});
            this.keysDown[key] = ts;
        }

        return this.keyHistory.splice(0, this.keyHistory.length));
    }
}
