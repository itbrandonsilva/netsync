import {BaseGateway} from "./BaseGateway";
import * as net from "net";

"use strict";

export class TCPGateway extends BaseGateway {
    constructor(port, maxConnections, host) {
        super();
        this.PORT = port || 8085;
        this.HOST = host || '127.0.0.1';
        this.max = maxConnections;
    }

    enable() {
        if (this.world.isServer) this.prepareServer();
        else                     this.prepareClient();
    }

    prepareTCPSocket(socket) {
        var self = this;
        var conn = this.registerConnection(function (msg) {
            socket.write(JSON.stringify(msg) + "\n\n");
        });

        socket.on('data', function (data) {
            var data = data.toString();
            var msg = data.toString();
            msg.split("\n\n").forEach(function (msg) {
                if (msg.length) self.handleMessage(JSON.parse(msg), conn);
            });
        });

        socket.on('error', function (error) {
            console.log('Connection error:');
            console.log(error);
        });

        socket.on('close', function () {
            console.log('Connection closed');
            self.destroyConnection(conn);
        });

        return conn;
    };


    prepareClient() {
        console.log('prepareClient()');
        var self = this;
        var socket = net.connect(this.PORT, this.HOST, function () {
            var conn = self.prepareTCPSocket(socket);
            self.gatewayReady();
        });
    }

    prepareServer() {
        console.log('prepareServer()');
        var self = this;
        var TCPServer = new net.createServer({}, function (socket) {
            var conn = self.prepareTCPSocket(socket);
        });
        TCPServer.listen(this.PORT, function () {
            console.log('Server listening...');
        });

        this.gatewayReady();
    }
}
