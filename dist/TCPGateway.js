var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseGateway_1 = require("./BaseGateway");
var net = require("net");
"use strict";
var TCPGateway = (function (_super) {
    __extends(TCPGateway, _super);
    function TCPGateway(port, maxConnections, host) {
        _super.call(this);
        this.PORT = port || 8085;
        this.HOST = host || '127.0.0.1';
        this.max = maxConnections;
    }
    TCPGateway.prototype.enable = function () {
        if (this.world.isServer)
            this.prepareServer();
        else
            this.prepareClient();
    };
    TCPGateway.prototype.prepareTCPSocket = function (socket) {
        var self = this;
        var conn = this.registerConnection(function (msg) {
            socket.write(JSON.stringify(msg) + "\n\n");
        });
        socket.on('data', function (data) {
            var data = data.toString();
            var msg = data.toString();
            msg.split("\n\n").forEach(function (msg) {
                if (msg.length)
                    self.handleMessage(JSON.parse(msg), conn);
            });
        });
        socket.on('error', function (error) {
            console.log('Connection error:');
            console.log(error);
        });
        socket.on('close', function () {
            console.log('Connection closed');
            self.destroyConnection(conn);
        });
        return conn;
    };
    ;
    TCPGateway.prototype.prepareClient = function () {
        console.log('prepareClient()');
        var self = this;
        var socket = net.connect(this.PORT, this.HOST, function () {
            var conn = self.prepareTCPSocket(socket);
            self.gatewayReady();
        });
    };
    TCPGateway.prototype.prepareServer = function () {
        console.log('prepareServer()');
        var self = this;
        var TCPServer = new net.createServer({}, function (socket) {
            var conn = self.prepareTCPSocket(socket);
        });
        TCPServer.listen(this.PORT, function () {
            console.log('Server listening...');
        });
        this.gatewayReady();
    };
    return TCPGateway;
})(BaseGateway_1.BaseGateway);
exports.TCPGateway = TCPGateway;
