var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseInput_js_1 = require("./BaseInput.js");
"use strict";
var SampleInput = (function (_super) {
    __extends(SampleInput, _super);
    function SampleInput(keys) {
        _super.call(this);
        this.keys = keys || ['w', 'a', 's', 'd', 'enter'];
        var keysDown = {};
        var keyHistory = [];
        this.keysDown = keysDown;
        this.keyHistory = keyHistory;
        var config = [];
        this.keys.forEach(function (key) {
            config.push({
                "keys": key,
                "prevent_repeat": true,
                "on_keydown": function () {
                    keyHistory.push({ key: key, ms: 0, down: true });
                    var ts = new Date().getTime();
                    keysDown[key] = ts;
                },
                "on_keyup": function () {
                    var ts = new Date().getTime();
                    keyHistory.push({ key: key, ms: ts - keysDown[key], up: true });
                    delete keysDown[key];
                },
            });
        });
        var interval = setInterval(function () {
            if (document.readyState != "complete")
                return;
            var listener = new keypress.Listener();
            var my_combos = listener.register_many(config);
            clearInterval(interval);
        }, 25);
    }
    SampleInput.prototype.sampleInputs = function () {
        var ts = new Date().getTime();
        for (var key in this.keysDown) {
            this.keyHistory.push({ key: key, ms: ts - this.keysDown[key] });
            this.keysDown[key] = ts;
        }
        return this.keyHistory.splice(0, this.keyHistory.length);
        ;
    };
    return SampleInput;
})(BaseInput_js_1.BaseInput);
exports.SampleInput = SampleInput;
