"use strict";
var BaseInput = (function () {
    function BaseInput() {
        this.inputs = [];
    }
    BaseInput.prototype.addInput = function (data) {
        this.inputs.push(data);
    };
    // Override this to have logic that provides inputs
    BaseInput.prototype.sampleInputs = function () { };
    //newInput(data) {
    //    return {type: 'input', /*inputId: this.world.genId(),*/ input: data};
    //}
    // Override this to clean up references
    BaseInput.prototype.destroy = function () { };
    ;
    return BaseInput;
})();
exports.BaseInput = BaseInput;
