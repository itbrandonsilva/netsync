"use strict";
var BaseEntity = (function () {
    function BaseEntity(params) {
        this.construct(params);
    }
    BaseEntity.prototype.construct = function (params) {
        this._map = ['inputType', 'entityId', 'owner'];
        params = params || {};
        if (params.world) {
            this.world = params.world;
            delete params.world;
        }
        this.setId(params.entityId);
        this.init(params);
        this.inputType = this.inputType || 'serverOnly';
    };
    BaseEntity.prototype.init = function (params) { };
    ;
    BaseEntity.prototype.entityType = function () {
        return this.constructor.name;
    };
    //static entityType() {
    //    return 'BaseEntity';
    //}
    BaseEntity.prototype.map = function (props) {
        this._map.push.apply(this._map, props);
    };
    //entityType() {
    //    return this.constructor.entityType();
    //}
    BaseEntity.prototype.setId = function (id) {
        if (!id)
            return;
        this.entityId = id;
    };
    BaseEntity.prototype.setOwner = function (playerId) {
        this.owner = playerId;
    };
    BaseEntity.prototype.getOwner = function () {
        return this.world.getPlayer(this.owner);
    };
    BaseEntity.prototype.handleEvent = function (event) { };
    ;
    BaseEntity.prototype.shouldProcessInput = function (fromServer) {
        switch (this.inputType) {
            case 'serverOnly':
                if (fromServer || this.world.isServer)
                    return true;
            case 'none':
            default:
                return false;
        }
    };
    BaseEntity.prototype.processInput = function (playerId, input) { };
    BaseEntity.prototype.createSnapshot = function (snapshot) {
        var _this = this;
        snapshot = snapshot || {};
        this._map.forEach(function (property) {
            snapshot[property] = _this[property];
        });
        snapshot.entityType = this.entityType();
        return snapshot;
    };
    //restoreSnapshot(snapshot) {
    //    this._map.forEach(property => {
    //        this[property] = snapshot[property];
    //    });
    //}
    BaseEntity.prototype.destroy = function () {
        delete this.world;
    };
    BaseEntity.prototype.setInputType = function (type) { this.inputType = this.inputType || 'serverOnly'; };
    return BaseEntity;
})();
exports.BaseEntity = BaseEntity;
