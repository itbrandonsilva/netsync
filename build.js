#!/usr/local/bin/node

// I couldn't find a way to get tsc to ignore types altogether. The browser was doing it just fine.

"use strict";

var fs = require('fs');
var path = require('path');
var typescript = require('typescript');

var cwd = process.cwd();

function loadConfig(configPath) {
    var tsconfig = require(configPath);
    process.chdir(path.dirname(configPath));

    var files = tsconfig.files.map(path => {
        var file = fs.readFileSync(path, 'utf8');
        var fileName = '/' + path.split('/').pop();
        return [file, fileName];
    });

    files.forEach(file => {
        //var transpiled = typescript.transpile(file[0], {target: 1, module: 4}, file[1]);
        var transpiled = typescript.transpile(file[0], {target: 1, module: tsconfig.compilerOptions.module}, file[1]);
        fs.writeFileSync(path.join(tsconfig.compilerOptions.outDir, file[1].split('.').shift() + '.js'), transpiled, 'utf8');
    });

    process.chdir(cwd);
}

loadConfig('./tsconfig.json');
loadConfig('./example/tsconfig.json');
